define(['angular', '../services'], function (angular, spekitService) {
	'use strict';
	
	return spekitService
	.factory('Regions', ['$resource', function($resource) {
	    return $resource('/api/region/:regionId', {
	    	regionId: '@id'
	    }, {
			query : {
				method : 'GET',
				isArray : true,
			},
	        update: {
	            method: 'PUT'
	        }
	    });
	}]);
});