define(['angular', '../services'], function (angular, spekitService) {
	'use strict';
	
	return spekitService
		.factory("Reports", ['$resource', function($resource) {
		    return $resource('/api/report/:reportId', {
		    	reportId: '@id'
		    }, {
		        update: {
		            method: 'PUT'
		        }
		    });
		}]);
});