define(['angular', '../services'], function (angular, spekitService) {
	'use strict';
	
	return spekitService
		.factory("AdminSchedule", ['$resource', function($resource) {
		    return $resource('/api/admin/schedule', {}, {
				query : {
					method : 'GET',
					isArray : true,
				},
				get: {
					url: '/api/admin/schedule/:actionId',
					method: 'GET'
				},
				create: {
					url: '/api/admin/schedule/:actionId',
					method: 'POST'
				},
		        update: {
					url: '/api/admin/schedule/:actionId',
		            method: 'PUT'
		        },
		        delete: {
					url: '/api/admin/schedule/:actionId',
		            method: 'DELETE'
		        }
		    });
		}]);
});