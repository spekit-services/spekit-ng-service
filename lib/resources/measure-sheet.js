//Jobs service used for Jobs REST endpoint
define(['angular', '../services'], function (angular, spekitService) {
	'use strict';
	
	return spekitService
		.factory("Measures", ['$resource', function($resource) {
		    return $resource('/api/measure-sheet/:measureId', {
		    	measureId: '@id'
		    }, {
				query : {
					method : 'GET',
					isArray : true,
				},
		        update: {
		            method: 'PUT'
		        },
		    });
		}]);
});
