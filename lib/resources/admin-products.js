define(['angular', '../services'], function (angular, spekitService) {
	'use strict';
	
	return spekitService
		.factory("Products", ['$resource', function($resource) {
		    return $resource('/api/admin/product/:productId', {
		    	productId: '@id'
		    }, {
				query : {
					method : 'GET',
					isArray : false,
				},
		        update: {
		            method: 'PUT'
		        },
		        queryAttributes : {		 
		        	url: '/api/product/:pId/attribute/:aId',
		        	params: {aId: '@aId', pId: '@pId'},
		        	method: 'GET',
		        	isArray : false,
		        },
		        getAttributeValues : {		 
		        	url: '/api/product/attribute/:attribute',
		        	params: { attribute: '@attribute' },
		        	method: 'GET',
		        	isArray : false,
				},
				
				
				getProcure:  {
					url: '/api/product/:id/procure',
					method: 'GET',
					isArray: true,
				},

				createProcure:  {
					url: '/api/product/procure',
					method: 'POST'
				},

				updateProcure:  {
					url: '/api/product/procure/:id',
					method: 'PUT'
				},
								
				getProductRules: {
					url: '/api/product/:id/rules',
					method: 'GET',
					isArray: false,
					transformResponse: function(data, headersGetter, status) {
						return {content: data};
					}
				},				

				updateProductRules: {
					url: '/api/product/:id/rules',
					method: 'PUT',
					isArray: false,
					headers: {'Content-Type': 'text/plain'}
				}
		    });
		}]);
});