//Projects service used for Projects REST endpoint
define(['angular', '../services'], function (angular, spekitService) {
	'use strict';
	
	return spekitService
		.factory("Projects", ['$resource', function($resource) {
			return $resource('/api/project/:projectId', {
				projectId: '@id'
			}, {
				update: {
					method: 'PUT'
				},
				getForChannel: {
					params: {channel: '@channelId'},
					method: 'GET',
				}
			});
		}]);
});