define(['angular', '../services'], function (angular, spekitService) {
	'use strict';

	return spekitService
		.factory("Global", [function() {
		    var _this = this;
		    _this._data = {
		        user: window.user,
		        authenticated: !! window.user
		    };
		
		    return _this._data;
		}]);
});