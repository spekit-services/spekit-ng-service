define(['angular', '../services'], function (angular, spekitService) {
	'use strict';

	return spekitService
	
	.factory('Workflow', ['$resource', function($resource) {
	    return $resource('/api/workflow/model/:modelId', {modelId: '@id'}, {
			query : {
				method : 'GET',
				isArray: true,
			},
			update : {
				method : 'PUT'
			},
			save : {
				url: '/api/workflow/model',
				method: 'POST'
			},
			get : {
				url: '/api/workflow/model/:id',
				params: {id: '@id'},
				method: 'POST'
			},
			getModelNames: {
				url: '/api/workflow/model-names',
				isArray: true,
			},
			
			createTask: {
				url: '/api/workflow/task',
				method: 'POST'
			},
			getTask: {
				url: '/api/workflow/task/:id',
				params: {id: '@id'},
				method: 'GET'
			},
			updateTask: {
				url: '/api/workflow/task/:id',
				params: {id: '@id'},
				method: 'PUT'
			},
			deleteTask: {
				url: '/api/workflow/task/:id',
				params: {id: '@id'},
				method: 'DELETE'
			},
		});
}]);
});
