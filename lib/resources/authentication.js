define(['angular', 'keycloak', '../services'], function (angular, Keycloak, spekitService) {
	'use strict';

	var DEBUG = false;		// console logs
	var AUTH_PREV_SESSION_HANDLE = 'AuthenticationPreviousSession';

	return spekitService

		/**
		 * Authentication service.  Performs login functions with the web service.
		 */
		.factory("Auth", ['$http', '$q', '$rootScope', '$location', '$window', 'AppBroadcast', function($http, $q, $rootScope, $location, $window, AppBroadcast) {
			// service paths
			var PATH_LOGIN  = "/api/j_security_check";
			var PATH_LOGOUT = "/api/auth/logout";
			var PATH_ROLES  = "/api/auth/roles";
			var PATH_ME     = "/api/auth/me";
			var AUTH_CHANGE_EVENT_NAME = "AUTH_EVENT";

			var loggingIn = false;
		    
			// Authentication info.
			var authInfo = {
				authenticated: undefined,
				user: {
					username: null,
					name: null,
					email: null,
				}
			};
			
			/**
			 * Please note that after adding a callback you must remove it - otherwise this will cause a memory leak.
			 * Generally removal is done by listening for the $destroy event and then calling Auth.removeAuthenticationCallback( callback )
			 */
			var observers = [];
			authInfo.addAuthenticationCallback = function(callback) {
				observers.push(callback);
			};
			authInfo.removeAuthenticationCallback = function(callback) {
				var indexOf = observers.indexOf(callback);
				if (indexOf >= 0) observers.splice(indexOf, 1);
			};
			authInfo.setAuthenticated = function(authenticated) {
				// Ensure this isn't called multiple times
				if (authInfo.authenticated === authenticated) return;

				// Otherwise update authenticated
				authInfo.authenticated = authenticated;

				if (!authenticated) {
					// reset user info to blank
					authInfo.user = undefined;
					window.localStorage.removeItem(AUTH_PREV_SESSION_HANDLE);
				}
				else {
					var prevSession = {
						idToken: authInfo.keycloak.idToken,
						refreshToken: authInfo.keycloak.refreshToken,
						token: authInfo.keycloak.token
					};

					window.localStorage.setItem(AUTH_PREV_SESSION_HANDLE, JSON.stringify(prevSession));
				}
				
				// Notify things on this app of change
				observers.forEach(function(callback) {
					callback(undefined, { authenticated : authInfo.authenticated, user: authInfo.user });
				});
				
				// Notify other tabs of change
				AppBroadcast.send(AUTH_CHANGE_EVENT_NAME, { action: authenticated ? 'Login' : 'Logout'});
			};

			
			/** 
			 * check whether user is authenticated (logged in).
			 */
			var check = (function() {
				var meRequestDeferred = null;		// if "me" request is already in progress, use the same $q.defer() object.

				return function() {
					if (DEBUG) {
						console.log("auth.check");
					}
					if (meRequestDeferred) {
						return meRequestDeferred.promise;
					}
					var defer = meRequestDeferred = $q.defer();

					if (authInfo.keycloak && authInfo.keycloak.authenticated) {
						$http.get(PATH_ME)         	// get "me" user details
							.then(function (res) {
								if (DEBUG) {
									console.log("auth.check: got user details", res.data);
								}
								authInfo.user = res.data;
								authInfo.setAuthenticated(true);
								meRequestDeferred = null;
								defer.resolve(res.data, res.status);
							}, function (res) {
								if (DEBUG) {
									console.log("auth.check: failed to get user details", res.data);
								}
								meRequestDeferred = null;
								defer.reject(res.data, res.status, res.headers, res.config);
							});
					}
					else {
							if (DEBUG) {
								console.log("auth.check: not authenticated");
							}
						// not authenticated
						meRequestDeferred = null;
						defer.reject();
					}
					
					return defer.promise;
				}
			})();

			/**
			 * Must be called when app is initialising
			 */
			authInfo.init = function(keycloak) {

				authInfo.keycloak = keycloak;
				keycloak.onReady = function(authenticated) { 
					authInfo.setAuthenticated(authenticated);
				};
				keycloak.onAuthSuccess = function() {
					authInfo.setAuthenticated(true);

					// TODO save login data
				};
				keycloak.onAuthLogout  = function() {
					authInfo.setAuthenticated(false);
				};
				keycloak.onAuthError = function() {
					console.log("AUTH ERROR!");
					window.localStorage.removeItem(AUTH_PREV_SESSION_HANDLE);
				};
				keycloak.onAuthRefreshError = function() {
					console.log("AUTH REFRESH ERROR!");
					window.localStorage.removeItem(AUTH_PREV_SESSION_HANDLE);
				};
				
				// Also register the broadcast event
				AppBroadcast.on(AUTH_CHANGE_EVENT_NAME, function(handle, event) {
					var newStatus = event.data.action === 'Login';
					console.log("Auth change: ", authInfo.authenticated , newStatus);
					if (authInfo.authenticated !== newStatus && newStatus === false && authInfo.loggingIn === false) { authInfo.logout(); }
					else if (authInfo.authenticated !== newStatus && newStatus === true) {
						window.location.reload();
					}
				});

				authInfo.setAuthenticated(keycloak.authenticated);
			};

			/** 
			 * get the current user 
			 */
			authInfo.getMe = function() {
				if (DEBUG) {
					console.log("auth.getMe");
				}
				if (authInfo.authenticated && authInfo.user && authInfo.user.id) {
					return $q.when(authInfo.user);
				}
				else {
					return check();
				}
			};
			
			/**
			 * Tries to get the previous auth session
			 * used for logging in across old tabs
			 */
			authInfo.getPreviousSession = function() {
				var value = window.localStorage.getItem(AUTH_PREV_SESSION_HANDLE);
				return value;
			}
			
			/** 
			 * get user roles 
			 */
			authInfo.getRoles = function(){
				var defer = $q.defer();
				
				$http.get(PATH_ROLES).then(function(res) {
					defer.resolve(res.data, res.status)
				}, function(res) {
					defer.reject(res.data, res.status, res.headers, res.config);
				});
			
				return defer.promise;
			};

			authInfo.redirectToLogin = function() {
				var redirectUri = $location.absUrl();
				var url = authInfo.keycloak.createLoginUrl();
				if (DEBUG) {
					console.log("Redirect to login", url, redirectUri);
				}
				//$window.location.href = url;
			};

			/** 
			 * send credentials in order to authenticate against service. 
			 */
			authInfo.login = function(credentials, callback) {
				console.log("Do login!")
				if (DEBUG) {
					console.log("keycloak: login");
				}
				if (authInfo.loggingIn) {
					return $q.reject("Already logging in");
				}
				authInfo.loggingIn = true;
				return authInfo.keycloak.login();
			};
			
			/** 
			 * logout of service. 
			 */
			authInfo.logout = function(callback) {
				if (DEBUG) {
					console.log("Auth: logout");
				}
				// Clear previous session handle
				window.localStorage.removeItem(AUTH_PREV_SESSION_HANDLE);
				// logout via keycloak
				return authInfo.keycloak.logout(/*{ redirectUri: ???? }*/);
			};
			
			/** 
			 * posts the current authinfo.user to spekit 
			 */
			authInfo.updateMe = function() {
				return $http.post(PATH_ME, this.user);
			};

			authInfo.subscribe = function(scope, callback) {
				var handler = $rootScope.$on('authentication-event', callback);
				authInfo.addAuthenticationCallback(callback);
				scope.$on('$destroy', function() { 
					handler();
					authInfo.removeAuthenticationCallback(callback);
				});	// remove event listener when scope is destroyed
				
				// send initial content
				$q.when(authInfo.keycloak).then(function() {
					callback(scope, { authenticated : authInfo.authenticated, user: authInfo.user } );
				});
			},


			/** 
			 * role check functions 
			 */
			authInfo.roles = {
				someOf: function(arg){
					var uRoles = authInfo.user.roles;				// roles of the user
					arg = typeof arg === "object" ? arg : [ arg ];	// roles to test against
					
					if (arg.length === 0) return true;
					for(var i in arg){
						for(var j in uRoles){
							if(arg[i] === uRoles[j]) return true;
						}
					}
					return false;
				},
				allOf: function(arg){
					var uRoles = authInfo.user.roles;				// roles of the user
					arg = typeof arg === "object" ? arg : [ arg ];	// roles to test against
					
					if (arg.length === 0) return true;
					if (arg.length > uRoles.length) {
						return false;
					}
					for (var i in arg) {
						var argument = arg[i];
						var found = uRoles.indexOf(argument);
						if (found === -1) {
							return false;
						}
					}
					return true;
				},
				noneOf: function(arg){
					var uRoles = authInfo.user.roles;
					arg = (arg instanceof Array) ? arg : [ arg ];

					if (arg.length === 0) return true;
					var result = true;
					arg.forEach(function(role) {
						uRoles.forEach(function(uRole) {
							if (role == uRole) {
								result = false;
							}
						});
					});
					
					return result;
				}
			};
			
			authInfo.rolesAsync = {
				someOf: function(arg) {
					// blocking function that takes args and returns if args are inside the users roles
					var fn = function(arg) {
						var uRoles = authInfo.user.roles;				// roles of the user
						arg = typeof arg === "object" ? arg : [ arg ];	// roles to test against
						for (var i in arg) {
							for (var j in uRoles) {
								if (arg[i] === uRoles[j]) { return true; }
							}
						}
						return false;
					};
					
					var defer = $q.defer();
					authInfo.getMe().then(function(success) {
						defer.resolve(fn(arg));
					}, function(error) {
						defer.resolve(false);
					});
					return defer.promise;
				}	
			};

			return authInfo;
		}]);
});