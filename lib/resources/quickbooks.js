/**
 * Vendros service used for Vendors REST endpoint
 */
define(['angular', '../services'], function (angular, spekitService) {
	'use strict';
	
	return spekitService
		.factory("Quickbooks", ['$resource', function($resource) {
		    return $resource('/api/qb', null, {
					get : {
						url : '/api/qb/vendor/:vendorId',
						method : 'GET',
					},
					update: {
						url : '/api/qb/vendor/:vendorId',
						method: 'PUT'
					},
					linkWithVendor : {
						url : '/api/qb/vendor/:vendorId/link',
						method : 'GET',
						responseType : 'text',
						transformResponse: function(data, headersGetter, status) {
							return { redirectUri: data };
						}
					},
					linkWithWebhooks : {
						url : '/api/qb/vendor/:vendorId/link-webhooks',
						method : 'PUT',
					},
					finishLinkWithVendor : {
						url : '/api/qb/vendor/:vendorId/link',
						method : 'PUT',
					},
					getVendorCompany : {
						url : '/api/qb/vendor/:vendorId',
						method : 'GET',
					},
					getVendorItems : {
						url : '/api/qb/vendor/:vendorId/items',
						method : 'GET',
						isArray : true,
					},
					getVendorTaxCodes : {
						url : '/api/qb/vendor/:vendorId/tax-code',
						method : 'GET',
						isArray : true,
					},
				});
		}]);
});
