define(['angular', '../services'], function (angular, spekitService) {
	'use strict';

	var DEBUG = false;		// console logs

	return spekitService

		/**
		 * permission provider.  Determines if the current user is authorised to perform a particular action.
		 */

		.provider("permission", function permissionProvider() {
			//default permissions object
			var permissions = {
					'useAdminTool'   : {type:'someOf', roles:['ADMIN']},
					'schedule'       : {type:'someOf', roles:['SCHEDULER', 'ADMIN']},
					'quote'          : {type:'someOf', roles:['CONSULTANT', 'VENDOR', 'ADMIN']},
					'editWorkflow'   : {type:'someOf', roles:['VENDOR', 'VENDOR_ADMIN', 'ADMIN']},
					'jobsByWorkflow' : {type:'someOf', roles:['VENDOR', 'VENDOR_ADMIN', 'ADMIN']},
					'manageReports'  : {type:'someOf', roles:['CONSULTANT_ADMIN', 'VENDOR_ADMIN', 'ADMIN']},
					'manageVendor'   : {type:'someOf', roles:['VENDOR_ADMIN', 'ADMIN']},
					'createNewTag'   : {type:'someOf', roles:['VENDOR_ADMIN', 'ADMIN']},
					'manageNotes'	 : {type:'someOf', roles:['VENDOR_ADMIN', 'ADMIN']},
			};
			
			var auth = {};
			
			this.addPermission = function(name, obj){
				permissions[name] = obj;
			};
			this.removePermission = function(name){
				if(permissions[name]) delete permissions[name];
			};
			
			this.permissionFunction = {
				can: function(task){
					var result = false;
					if (auth.authenticated) {
						var taskP = permissions[task];
						if (taskP) {
							var userRoles = auth.user.roles;
							result = auth.roles[taskP.type](taskP.roles);
						}
					}
					return result;
				},
				
				canAsync: function() {
					//return a promise
				}
			};
			
			this.$get = ['Auth', function PermissionFactory(Auth) {
				auth = Auth;
				return this.permissionFunction;
			}];
		});
});