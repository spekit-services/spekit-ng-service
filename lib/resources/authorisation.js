define(['angular', '../services'], function (angular,  spekitService) {
	'use strict';

	var DEBUG = false;

	return spekitService
	/**
	 * Authorisation object.  Check whether current user is authorised to access the current page/view.
	 */
	.factory('Authorisation', ['$rootScope', '$window', 'Auth', '$route', '$location', '$q', function($rootScope, $window, Auth, $route, $location, $q) {
		var requiredRoles = []; // Roles that the user must have all of to view
		var optionalRoles = []; // Roles that the user must have one of to view
		var publicPage    = 'PUBLIC_PAGE';    // All users can view this page
		var viewState     = 'OK';

		// Settings that allow the user to view the webpage
		var isAuthorised = false;
		var stateChanger = $q.defer();

		var changeAuthorisedStateTo = function(authorised) {
			if (DEBUG) {
				console.log("Authorisation: change authorisation state to " + authorised);
			}
			isAuthorised = authorised;
			stateChanger.notify(authorised);
			notify(authorised);
		};
		
		var pageIsPublic = function() {
			return $route.current && $route.current.$$route && $route.current.$$route.public;
		};
		
		var checkRoles = function() {
			return Auth.roles.allOf(requiredRoles) && Auth.roles.someOf(optionalRoles);
		};
		
		var processAuthorisation = function(data) {
			
			// We aren't authenticated, redirect to login
			if (!Auth.authenticated) {
					if (DEBUG) {
						console.log("ASuthorisation: processAuthorisation and not authenticated");
					}

				// Is this page public?		
				// Dont require Auth
				if (pageIsPublic()) {
					// If it's public there's no need for any authorisation
					
					// Check if the user first went to signin page
					// Set up a redirect
					if ($location.path() === '/signin') {
						addRedirectTo($route, '/');
					}
					
					// Eyesore of conditions, refactor this.
					if (Auth.authenticated && checkRoles()) {
						viewState = 'OK';
					}
					else {
						viewState = 'PUBLIC_PAGE';
					}
					changeAuthorisedStateTo(true);
					return;
				}
				
				// Otherwise this page isn't public. Do require auth
				else {
					// redirect to login page
					if (DEBUG) {
						console.log("redirect to login page???");
					}
					
					changeAuthorisedStateTo(false);
					return;
				}
			}
			
			// Ensure that we've managed to contact the auth api
			// TODO
			console.log("Have we got auth roles: ", Auth);
			if (Auth.authenticated && Auth.user.username === null) {
				viewState = 'FORBIDDEN';
				$location.path('/503').replace();
				changeAuthorisedStateTo(true);
				return;

			}
			// Are there any roles we must have?
			else if (!checkRoles()) {
				viewState = 'FORBIDDEN';
				$location.path('/403').replace();
				changeAuthorisedStateTo(true);
				return;
			}
			
			// We landed on signin while being logged in, redirect.
			if (Auth.authenticated && $location.path() === '/signin') {
				$location.path('/');
			}
			
			viewState = 'OK';
			changeAuthorisedStateTo(true);
		};
		
		/**
		 * Notify subscribed listeners whether user is authorised for current context
		 */
		var notify = function(isAuthorised) {
            $rootScope.$emit('authorise-event', { authorised : isAuthorised });
        }

		var Authorisation = {
			/**
			 * check whether current user has authorisation for the page in context.
			 */
			checkAuthorisation: function() {
				var deferred = $q.defer();
				//changeAuthorisedStateTo(false);			// why do this???
				Auth.getMe().then(function(user){
					if (DEBUG) {
						console.log("Authorisation.checkAuthorisation: got user", user);
					}
					processAuthorisation(user);
					deferred.resolve({user: user, isAuthorised: isAuthorised});
				}, function(data) {
					if (DEBUG) {
						console.log("Authorisation.checkAuthorisation: getMe error", data);
					}
					processAuthorisation(data);
					deferred.resolve({data: data, isAuthorised: isAuthorised});
				});
				
				return deferred.promise;
			},
			onStateChange: function() {
				return stateChanger.promise;
			},
			subscribe: function(scope, callback) {
				var handler = $rootScope.$on('authorise-event', callback);
				scope.$on('$destroy', handler);	// remove event listener when scope is destroyed
				callback(scope, { authorised : isAuthorised });	// send initial content
			},
			viewState: function() {
				return viewState;
			},
			setAppPermissionsRequired: function(param) {
				if (param.allOf) requiredRoles = param.allOf;
				if (param.anyOf) optionalRoles = param.anyOf;
			}
		};

		// track when user is authenticated or not
		Auth.subscribe($rootScope, function(event, auth) {
			if (DEBUG) {
				console.log("Authorisation: receved authentication event", auth);
			}
			Authorisation.checkAuthorisation();
		});

		return Authorisation;
	}])
});