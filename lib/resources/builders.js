/**
 * Vendros service used for Vendors REST endpoint
 */
define(['angular', '../services'], function (angular, spekitService) {
	'use strict';
	
	return spekitService
		.factory("Builders", ['$resource', function($resource) {
		    return $resource('/api/builder/:builderId', {
					vendorId : '@id'
				}, {
					query : {
						method : 'GET',
						isArray : false,
					},
					update : {
						method : 'PUT',
					},
					getHouses: {
						params: {builderId: '@builderId'},
						method: 'GET',
						url: '/api/builder/:builderId/houses',
						isArray: true,
					},
					getHouseRanges: {
						params: {builderId: '@builderId'},
						method: 'GET',
						url: '/api/builder/:builderId/house-ranges',
						isArray: true,
					},
			        getSettingTags: {
						params: {builderId: '@builderId'},
						url: '/api/builder/:builderId/tags/setting',
						method: 'GET',
						isArray: true
			        },
			        persistSettingTag: {
						params: {builderId: '@builderId'},
			        	url: '/api/builder/:builderId/tags/setting',
			        	method: 'POST'
			        }
				});
		}]);
});
