define(['angular', '../services'], function (angular, spekitService) {
	'use strict';
	
	return spekitService
		.factory("Dwellings", ['$resource', function($resource) {
		    return $resource('/api/house/:dwellingId', {
		    	dwellingId: '@id'
		    }, {
				query : {
					method : 'GET',
					isArray : false,
				},
		        update: {
		            method: 'PUT'
		        }
		    });
		}]);
});