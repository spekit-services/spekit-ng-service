/**
 * Vendros service used for Vendors REST endpoint
 */
define(['angular', '../services'], function (angular, spekitService) {
	'use strict';
	
	return spekitService
		.factory("Zone", ['$resource', function($resource) {
		    return $resource('/api/geo/zone', null, {
					query : {
						method : 'GET',
						isArray : true,
					},
				});
		}]);
});
