/**
 * package service used for product packages REST endpoint
 */
define(['angular', '../services'], function (angular, spekitService) {
	'use strict';
	
	return spekitService
		.factory("Packages", ['$resource', function($resource) {
		    return $resource('/api/admin/package/:packageId', {
		        packageId: '@packageId', dwellingId: '@dwellingId'
		    }, {
				query : {
					method : 'GET',
					isArray : true,
				},
		        update: {
		            method: 'PUT'
		        },
		        getHousePkg : {
		        	url : '/api/admin/package/:packageId/dwelling/:dwellingId',
		        	method : 'GET'
		        },
		        create : {
		        	url: '/api/admin/package/',
		        	method: 'POST',
				},
				
		        update : {
		        	url: '/api/admin/package/:packageId',
		        	method: 'PUT',
		        }
		    });
		}]);
});