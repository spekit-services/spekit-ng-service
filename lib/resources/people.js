/**
 * Customers service used for Customers REST endpoint
 */
define(['angular', '../services'], function (angular, spekitService) {
	'use strict';

	return spekitService
		.factory("People", ['$resource', function($resource) {
		    return $resource('/api/person', {}, {
					query : {
						method : 'GET',
						isArray : true,
					},
					update : {
						method : 'PUT'
					}
				});
		}]);
});