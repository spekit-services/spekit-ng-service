define(['angular', '../services'], function (angular, spekitService) {
	'use strict';
	
	return spekitService
		.factory("ProductComponents", ['$resource', function($resource) {
		    return $resource('/api/product/component/:componentId', {
		    	componentId: '@id'
		    }, {
				query : {
					method : 'GET',
					isArray : false,
				},
		        update: {
		            method: 'PUT'
		        }
		    });
		}])
		.factory("ProductComponentTypes", ['$resource', function($resource) {
		    return $resource('/api/product/component-type/:typeId', {
		    	typeId: '@id'
		    }, {
				query : {
					method : 'GET',
					isArray : true,
				},
		        update: {
		            method: 'PUT'
		        }
		    });
		}]);
});