define(['angular', '../services'], function (angular, spekitService) {
	'use strict';
	
	return spekitService
		.factory("Tasks", ['$resource', function($resource) {
		    return $resource('/api/event/:taskId', {
		    	taskId: '@id'
		    }, {
				query : {
					method : 'GET',
					isArray : false,
				},
		        update: {
		            method: 'PUT'
		        }
		    });
		}]);
});