/**
 * Vendors service used for Vendors REST endpoint
 */
define(['angular', '../services'], function (angular, spekitService) {
	'use strict';
	
	return spekitService
		.factory("Vendors", ['$resource', function($resource) {
		    return $resource('/api/admin/vendor/:vendorId', {
					vendorId : '@id'
				}, {
					query : {
						method : 'GET',
						isArray : false,
					},
					update : {
						method : 'PUT'
					},
					getAttachmentTags: {
						params: {vendorId: '@vendorId'},
						method: 'GET',
						url: '/api/vendor/:vendorId/tags/attachment',
						isArray: true,
					},
					persistAttachmentTag: {
						params: {vendorId: '@vendorId'},
						method: 'POST',
						url: '/api/vendor/:vendorId/tags/attachment'
					},
					getJobTags: {
						params: {vendorId: '@vendorId'},
						method: 'GET',
						url: '/api/vendor/:vendorId/tags/job',
						isArray: true,
					},
					persistJobTag: {
						params: {vendorId: '@vendorId'},
						method: 'POST',
						url: '/api/vendor/:vendorId/tags/job',
					},
					removeTag: {
						params: {tagId : '@tagId'},
						url: '/api/admin/tag/:tagId',
						method: 'DELETE'
					},
					enableQuickbooksIntegration: {
						params: {vendorId : '@vendorId'},
						url: '/api/admin/vendor/:vendorId/qb',
						method: 'PUT'
					}
				});
		}]);
});