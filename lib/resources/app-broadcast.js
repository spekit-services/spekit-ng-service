define(['angular', '../services'], function (angular, spekitService) {
	'use strict';

	return spekitService

	.factory('AppBroadcast', appBroadcaster);
	
		function appBroadcaster () {
			var vm = {};

			var broadcasters = 	{};
			var callbacks = 		{};

			vm.send = 				send;
			vm.on = 					onMessage;

			function getHandle(thread, i) {
				return thread+"~"+i;
			}

			function handleMessageFromChannel(ev) {
				var thread = ev.target && ev.target.name;
				console.log("Message from : "+thread);
				if (thread !== undefined && callbacks[thread]) {
					callbacks[thread].forEach(function(callback, i) {
						callback(getHandle(thread, i), ev);
					});
				}
			}

			function get(thread) {
				// Check to see if broadcaster exists 
				if (thread in broadcasters) {
					return broadcasters[thread];
				}
				else if (window.BroadcastChannel) {
					var channel = new window.BroadcastChannel(thread);
					channel.onmessage = handleMessageFromChannel;
					return broadcasters[thread] = channel;
				}
			}
			function send(thread, message) {
				var caster = get(thread);
				if (caster && caster.postMessage) {
					console.log("AppBroadcaster.send::"+thread+"=", message);
					caster.postMessage(message);
				}
				else {
					console.error('AppBroadcaster.send:: Failed to fetch broadcaster (Are you using an unsopported browser?)');
				}
			}
			function onMessage(thread, callback) {
				console.log("appBroadcaster.onMessage::"+thread);

				get(thread); // Load in the broadcaster if it doesnt exist
				
				if (!callbacks[thread]) callbacks[thread] = [callback];
				else callbacks[thread].push(calback);

				return getHandle(thread, callbacks[thread].length);
			};

			return vm;
		}
});