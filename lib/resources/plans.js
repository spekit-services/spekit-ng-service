//Jobs service used for Jobs REST endpoint
define(['angular', '../services'], function (angular, spekitService) {
	'use strict';
	
	return spekitService
		.factory("Plans", ['$resource', function($resource) {
		    return $resource('/api/plan/:planId', {
		        channelId: '@id'
		    }, {
				query : {
					method : 'GET',
					isArray : false,
				},
		        update: {
		            method: 'PUT'
		        }
		    });
		}]);
});