//Jobs service used for Jobs REST endpoint
define(['angular', '../services'], function (angular, spekitService) {
	'use strict';
	
	return spekitService
		.factory("Groups", ['$resource', function($resource) {
		    return $resource('/api/group/:groupId', {
		        groupId: '@id'
		    }, {	
		    	
				query: {
					method: 'GET',
					isArray: true
				},
				
				create: {
					method: 'POST',
					url: '/api/group'
				},
				
				update: {
					params: {groupId: '@groupId'},
					method: 'PUT',
					url: '/api/group/:groupId'
				},
							
				get: {
					params: {groupId: '@groupId'},
					method: 'GET',
					url: '/api/group/:groupId'
				},
				
				getUsers: {
					params: {groupId: '@groupId'},
					method: 'GET',
					url: '/api/group/:groupId/users',
					isArray: true
				},
				
				setUsers: {
					params: {groupId: '@groupId'},
					method: 'PUT',
					url: '/api/group/:groupId/users',
					isArray: true
				},
		    });
		}]);
});
