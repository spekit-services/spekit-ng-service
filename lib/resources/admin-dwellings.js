define(['angular', '../services'], function (angular, spekitService) {
	'use strict';
	
	return spekitService
		.factory("Dwellings", ['$resource', function($resource) {
		    return $resource('/api/admin/house/:dwellingId', {
		    	dwellingId: '@id'
		    }, {
				query : {
					method : 'GET',
					isArray : false,
				},
		        update: {
		            method: 'PUT'
		        },
		        removePackage: {
		        	params: {packageId: '@packageId', dwellingId: '@dwellingId'},
					method: 'DELETE',
					url: '/api/admin/package/:packageId/dwelling/:dwellingId'
		        }
		    });
		}]);
});