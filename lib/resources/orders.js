//Orders service used for Orders REST endpoint
define(['angular', '../services'], function (angular, spekitService) {
	'use strict';
	
	return spekitService
		.factory("Orders", ['$resource', function($resource) {
		    return $resource('/api/order/:orderId', {
		    	orderId: '@id'
		    }, {
			    query: {
			    	method: 'GET',
			    	isArray: false
			    },
		        update: {
		            method: 'PUT'
		        },
		    	getPdf: {
		    		method: 'GET',
		    		url: '/api/order/:orderId.pdf'
		    	},
		    	getMeasureSheet: {
		    		method: 'GET',
		    		url: '/api/order/:orderId/installation.pdf'
		    			
		    	},
		    	updateOrderPrice: {
		    		params: {'orderId':'orderId'},
		    		method: 'PUT',
		    		url: '/api/order/:orderId/price',
		    	},
		    	updateOrderDiscount: {
		    		params: {orderId:'orderId'},
		    		method: 'PUT',
		    		url: '/api/order/:orderId/price/discount',
		    	},
		    	getInstallSheet: {
		    		method: 'GET',
		    		url: '/api/order/:orderId/check-measure.pdf'
		    	},
		    	updateStatus: {
		    		method: 'PUT',
		    		url: '/api/order/:orderId/status',
		    		transformRequest: function(data){
		    			var result = '"' + data.status + '"';
		    			return result;
		    		}
		    	}
		    });
		}]);
});