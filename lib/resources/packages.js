/**
 * package service used for product packages REST endpoint
 */
define(['angular', '../services'], function (angular, spekitService) {
	'use strict';
	
	return spekitService
		.factory("Packages", ['$resource', function($resource) {
		    return $resource('/api/package/:packageId', {
		        packageId: '@packageId', dwellingId: '@dwellingId'
		    }, {
				query : {
					method : 'GET',
					isArray : true,
				},
		        update: {
		            method: 'PUT'
		        },
				applyHousePackage: {
		        	url: '/api/package/:packageId/apply',
		            method: 'PUT'
				}
		    });
		}]);
});