//Jobs service used for Jobs REST endpoint
define(['angular', '../services'], function (angular, spekitService) {
	'use strict';
	
	return spekitService
		.factory("Jobs", ['$resource', function($resource) {
		    return $resource('/api/job/:jobId', {
		        jobId: '@id'
		    }, {
				query : {
					method : 'GET',
					isArray : false,
				},
		        update: {
		            method: 'PUT'
		        },
				updateSite: {
					params: {jobId: '@jobId'},
					method: 'PUT',
		            url: '/api/job/:jobId/site'
				},
				updateSiteAddress: {
					params: {jobId: '@jobId'},
					method: 'PUT',
		            url: '/api/job/:jobId/site/address'
				},
				updateSiteManager: {
					method: 'POST',
		            url: '/api/job/:jobId/site/manager'
				},
				updateJobNumber: {
					method: 'PUT',
		            url: '/api/job/:jobId/number',
		    		transformRequest: function(data){
		    			return data.jobNumber;
		    		}
				},
				updateJobPrice: {
					method: 'PUT',
		            url: '/api/job/:jobId/price',
		    		transformRequest: function(data){
		    			return data.totalPrice;
		    		}
				},
				updateJobStatus: {
					method: 'PUT',
		            url: '/api/job/:jobId/status',
		    		transformRequest: function(data){
		    			return '\"'+data.status+'\"';
		    		}
				},
				
	        	getJobEvents: {
					method : 'GET',
					url: '/api/job/:jobId/event',
					isArray : true,
	        	},
	        	
				createJournal: {
					method: 'POST',
					headers: {'Content-Type': 'text/plain'},
					url: '/api/job/:jobId/note',
		    		transformRequest: function(data){
		    			var result = data.comment;
		    			return result;
		    		}
				},
				
				addTag: {
					method: 'POST',
					url: '/api/job/:jobId/tag',
				},
				deleteTag: {
					method: 'DELETE',
					url: '/api/job/:jobId/tag/:tagId',
				},
				saveTask: {
					method: 'POST',
					url: '/api/job/:jobId/task',
//		    		transformRequest: function(data){
//		    			var result = data;
//		    			result.id = data.taskId;
//		    			delete result.taskId;
//		    			return result;
//		    		}
				},
				/** THESE SHOULD BE IN THE event SERVICE */
				saveEvent: {
					method: 'POST',
					url: '/api/event'
				},
				
				//attachment of files to job
				assignFile: {
					params: {jobId: '@jobId'},
					method: 'POST',
					url: '/api/job/:jobId/attachment'
				},
				
				updateFileDescription: {
					params: {fileId: '@id'},
					method: 'PUT',
					url: '/api/file/:fileId/description',
					transformRequest: function(data){
						return data.description;
					}
				},
				
				persistTag: {
					params: {jobId: '@jobId'},
					method: 'POST',
					url: '/api/job/:jobId/tag',
					isArray: true
				},
				
				removeTag: {
					params: {jobId: '@jobId', tagId: '@tagId'},
					method: 'DELETE',
					url: '/api/job/:jobId/tag/:tagId'
				},
				
				getTags: {
					params: {jobId: '@jobId'},
					method: 'GET',
					url: '/api/job/:jobId/tag',
					isArray: true
				},
				
				generateJobNumber: {
					params: {channelId: '@channelId'},
					headers: {'Content-Type':'text/plain'},
					method: 'POST',
					url: '/api/job/number/:channelId',
					transformResponse: function(data) {
						return {jobNumber: angular.fromJson(data)};
					}
				},
				
				getAttachments: {
					params: {jobId: '@jobId'},
					method: 'GET',
					url: '/api/job/:jobId/attachment',
					isArray: true
				},
				
				getMeasures: {
					params: {jobId: '@jobId'},
					method: 'GET',
					url: '/api/job/:jobId/measure-sheet',
					isArray: true
				},

				getOrders: {
					params: {jobId: '@jobId'},
					method: 'GET',
					url: '/api/job/:jobId/order',
					isArray: true
				}
		    });
		}]);
});
