define(['angular', '../services'], function (angular, spekitService) {
	'use strict';
	
	return spekitService
	.factory('HouseRanges', ['$resource', function($resource) {
	    return $resource('/api/house-range/:rangeId', {
	    	rangeId: '@id'
	    }, {
			query : {
				method : 'GET',
				isArray : true,
			},
	        update: {
	            method: 'PUT'
	        },
			find : {
				url: '/api/house-range',
				method: 'GET',
				isArray: true
			}
	    });
	}]);
});