define(['angular', '../services'], function (angular, spekitService) {
	'use strict';
	
	return spekitService
		.factory("Products", ['$resource', function($resource) {
		    return $resource('/api/product/:productId', {
		    	productId: '@id'
		    }, {
				query : {
					method : 'GET',
					isArray : true,
				},
		        update: {
		            method: 'PUT'
		        },
		        queryAttributes : {		 
		        	url: '/api/product/:pId/attribute/:aId',
		        	params: {aId: '@aId', pId: '@pId'},
		        	method: 'GET',
		        	isArray : false,
		        },
		        getAttributeValues : {		 
		        	url: '/api/product/attribute/:attribute',
		        	params: { attribute: '@attribute' },
		        	method: 'GET',
		        	isArray : false,
				},

				getProductOrderResult:  {
					url: '/api/product/:id/procure/:orderId',
					method: 'GET',
					isArray: true
				},
		    });
		}]);
});