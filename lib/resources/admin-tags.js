define(['angular', '../services'], function (angular, spekitService) {
	'use strict';
	
	return spekitService
		.factory("Tags", ['$resource', function($resource) {
		    return $resource('/api/admin/tag/:tagId', {
		    	tagId: '@id'
		    }, {
				query : {
					method : 'GET',
					isArray : true,
				},
		        update: {
		            method: 'PUT'
		        }
		    });
		}]);
});