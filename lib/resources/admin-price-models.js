/**
 * models service used for Price Models REST endpoint
 */
define(['angular', '../services'], function (angular, spekitService) {
	'use strict';
	
	return spekitService
		.factory("PriceModels", ['$resource', function($resource) {
		    return $resource('/api/price/model/:modelId', {
		        modelId: '@id', tableId: '@tableId'
		    }, {
				query : {
					method : 'GET',
					isArray : true,
				},
		        update: {
		            method: 'PUT'
		        },
				queryTable:{
		        	url : '/api/admin/price/table/:tableId',
		        	method: 'GET',
					isArray : true
				},
				updateTable:{
		        	url : '/api/admin/price/table',
		        	method: 'POST'
				},
				deleteTable:{
					url : '/api/admin/price/table/:tableId',
					method : 'DELETE'
				}
		    });
		}]);
});