/**
 * Vendros service used for Vendors REST endpoint
 */
define(['angular', '../services'], function (angular, spekitService) {
	'use strict';
	
	return spekitService
		.factory("Suppliers", ['$resource', function($resource) {
		    return $resource('/api/supplier/:supplierId', {
					supplierId : '@id'
				}, {
					query : {
						method : 'GET',
						isArray : false,
					},
					update : {
						method : 'PUT'
					}

				});
		}]);
});
