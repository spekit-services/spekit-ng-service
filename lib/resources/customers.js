/**
 * Customers service used for Customers REST endpoint
 */
define(['angular', '../services'], function (angular, spekitService) {
	'use strict';

	return spekitService
		.factory("Customers", ['$resource', function($resource) {
		    return $resource('/api/customer/:customerId', {
					customerId : '@id'
				}, {
					query : {
						method : 'GET',
						isArray : false,
					},
					update : {
						method : 'PUT'
					},
					getJobs : {
						url: '/api/customer/:customerId/jobs',
						method: 'GET',
						isArray: true
					}
				});
		}]);
});