define(['angular', '../services'], function (angular, spekitService) {
	'use strict';
	
	return spekitService
		.factory("Files", ['$resource', function($resource) {
		    return $resource('/api/file/:fileId', {
		    	fileId: '@id'
		    }, {
				query : {
					method : 'GET',
					isArray : false,
				},
		        update: {
		            method: 'PUT'
		        },
				updateDescription: {
					params: {fileId: '@id'},
					method: 'PUT',
					url: '/api/file/:fileId/description',
					transformRequest: function(data){
						return data.description;
					}
				}
		    });
		}]);
});