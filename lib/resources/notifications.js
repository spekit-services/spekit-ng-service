define(['angular', '../services'], function (angular, spekitService) {
	'use strict';
	
	return spekitService
		.factory("Notifications", ['$resource', function($resource) {
		    return $resource('/api/notification/:notificationId', {
		    	notificationId: '@id'
		    }, {
				query : {
					method : 'GET',
				},
		        update: {
		            method: 'PUT'
		        }
		    });
		}]);
});