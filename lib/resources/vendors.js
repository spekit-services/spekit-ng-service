/**
 * Vendros service used for Vendors REST endpoint
 */
define(['angular', '../services'], function (angular, spekitService) {
	'use strict';
	
	return spekitService
		.factory("Vendors", ['$resource', function($resource) {
		    return $resource('/api/vendor/:vendorId', {
					vendorId : '@id'
				}, {
					query : {
						method : 'GET',
						isArray : false,
					},
					update : {
						method : 'PUT'
					},
					getAttachmentTags: {
						params: {vendorId: '@vendorId'},
						method: 'GET',
						url: '/api/vendor/:vendorId/tags/attachment',
						isArray: true,
					},
					getJobTags: {
						method: 'GET',
						url: '/api/vendor/:vendorId/tags/job',
						isArray: true,
					},
					persistJobTag: {
						params: {vendorId: '@vendorId'},
						method: 'POST',
						url: '/api/vendor/:vendorId/tags/job',
					}
				});
		}]);
});
