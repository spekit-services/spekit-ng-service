define(['angular', '../services'], function (angular, spekitService) {
	'use strict';
	
	return spekitService
		.factory("Users", ['$resource', function($resource) {
		    return $resource('/api/admin/user/:userId', {
		    	userId: '@id'
		    }, {
				query : {
					method : 'GET',
					isArray : false,
				},
		        update: {
		            method: 'PUT'
		        }
		    });
		}]);
});