/**
 * Customers service used for Customers REST endpoint
 */
define(['angular', '../services'], function (angular, spekitService) {
	'use strict';

	return spekitService
		.factory("Budgets", ['$resource', function($resource) {
		    return $resource('/api/budget/:budgetId', {
		    	budgetId : '@id'
				}, {
					query : {
						method : 'GET',
						isArray : true,
					},
					update : {
						method : 'PUT'
					},
			        queryItems : {		 
			        	url: '/api/budget/:budgetId/item/',
			        	params: {budgetId: '@id'},
			        	method: 'GET',
			        	isArray : true
			        	// Query params:
			        	// vendor: Long
						// channel: Long
						// user: Long
						// from: ISO8601Date
						// to: ISO8601Date
			        },
			        saveItem : {
			        	url: '/api/budget/:budgetId/item/',
			        	params: {budgetId: '@id'},
			        	method: 'POST'
			        }
				});
		}])
		.factory("BudgetItems", ['$resource', function($resource) {
		    return $resource('/api/budget/item/:itemId', {
		    	itemId : '@id'
				}, {
					query : {
						method : 'GET',
						isArray : true,
					},
					update : {
						method : 'PUT'
					},
				});
		}]);
});