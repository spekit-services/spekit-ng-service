/**
 * Jobs service used for Jobs REST endpoint
 */
define(['angular', '../services'], function (angular, spekitService) {
	'use strict';
	
	return spekitService
		.factory("Categories", ['$resource', function($resource) {
		    return $resource('/api/category/:categoryId', {
		        categoryId: '@id'
		    }, {
				query : {
					method : 'GET',
					isArray : true,
				},
		        update: {
		            method: 'PUT'
		        }
		    });
		}]);
});