define(['angular', '../services'], function (angular, spekitService) {
	'use strict';
	
	return spekitService
		.factory("Properties", ['$resource', function($resource) {
		    return $resource('/api/admin/property', {}, {
				query : {
					method : 'GET',
					isArray : true,
				},
				fetch: {
					params: {propertyName: '@name'},
					url: '/api/admin/property/:propertyName',
					method: 'GET'
				},
				create: {
					params: {propertyName: '@name'},
					url: '/api/admin/property/:propertyName',
					method: 'PUT'
				},
		        update: {
		        	params: {propertyName: '@name'},
					url: '/api/admin/property/:propertyName',
		            method: 'POST'
		        }
		    });
		}]);
});