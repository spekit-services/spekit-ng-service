/**
 * Service used for Channels REST endpoint
 */
define(['angular', '../services'], function (angular, spekitService) {
	'use strict';
	
	return spekitService
		.factory("Channels", ['$resource', function($resource) {
			return $resource('/api/channel/:channelId', {
				channelId: '@id'
			}, {
				query : {
					method : 'GET',
					isArray : false,
				},
				update: {
		            method: 'PUT'
				},
				getProjects: {
					method: 'GET',
					url: '/api/channel/:channelId/project',
				}
			});
		}]);
});