/**
 * Service used for Channels REST endpoint
 */
define(['angular', '../services'], function (angular, spekitService) {
	'use strict';
	
	return spekitService
		.factory("Channels", ['$resource', function($resource) {
		    return $resource('/api/admin/channel/:channelId', {
		        channelId: '@id'
		    }, {
				query : {
					method : 'GET',
					isArray : false,
				},
		        update: {
		            method: 'PUT'
		        },
		        /* UNUSED
		        getSettingTags: {
		        	params: {channelId: '@channelId'},
					url: '/api/channel/:channelId/tags/setting',
					method: 'GET',
					isArray: true
		        },
		        persistSettingTag: {
		        	params: {channelId: '@channelId'},
		        	url: '/api/channel/:channelId/tags/setting',
		        	method: 'POST'
		        }
		        */
		    });
		}]);
});