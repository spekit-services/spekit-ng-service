/**
 * models service used for Price Models REST endpoint
 */
define(['angular', '../services'], function (angular, spekitService) {
	'use strict';
	
	return spekitService
		.factory("Price", ['$resource', function($resource) {
		    return $resource('/api/consultant/prices', {
		    }, {
		    	// a single product configuration
		        getPrice: {
		        	url : '/api/consultant/price',
		            method: 'PUT'
		        },
		        
		        // an array of product configurations
		        getPrices: {
		        	url : '/api/consultant/prices',
		            method: 'PUT'
		        }
		    });
		}]);
});