/**
 * Defines Angular module "spekit.service"
 */
define(['angular',
        './resources/authentication',
        './resources/authorisation',
        './resources/app-broadcast',
        './resources/permission',
        
        './resources/categories', 
        './resources/channels', 
        './resources/customers', 
        './resources/dwellings', 
        './resources/files', 
        './resources/global', 
        './resources/jobs', 
        './resources/orders', 
        './resources/packages',
        './resources/plans',
        './resources/price',
        './resources/price-models', 
        './resources/products',
        './resources/product-components', 
        './resources/people',

        './resources/builders',
        './resources/suppliers', 
        './resources/projects',
        './resources/reports',
        './resources/tasks', 
        './resources/users',
        './resources/vendors',
        './resources/notifications',
        './resources/measure-sheet',
        './resources/regions',
        './resources/house-ranges',
        './resources/workflow',
        
        './resources/zone',
        './resources/budget',
        
        './resources/quickbooks',
//        './resources/version',
	], 
	function (angular) {
		'use strict';
                return angular.module('spekit.service').value('version', '0.1');
	}
);