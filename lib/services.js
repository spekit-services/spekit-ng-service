define(['angular'], 
	function (angular) {
		'use strict';		

		return angular.module('spekit.service', []);
	}
);