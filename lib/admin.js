/**
 * Defines Angular module "spekit.service"
 */
define(['angular',
        './resources/authentication',
        './resources/authorisation',
        './resources/app-broadcast',
        './resources/permission',
        
        './resources/admin-dwellings', 
        './resources/admin-packages', 
        './resources/admin-price-models', 
        './resources/admin-products', 
        './resources/admin-channels', 
        './resources/users',
        './resources/admin-vendors',
        './resources/admin-tags',
        './resources/admin-properties',
        './resources/admin-schedule',

        './resources/suppliers',
        './resources/builders',
        './resources/categories', 
        './resources/customers', 
        './resources/files', 
        './resources/global', 
        './resources/jobs', 
        './resources/orders', 
        './resources/plans',
        './resources/price',
        './resources/product-components', 
        './resources/projects',
        './resources/reports',
        './resources/tasks',
        './resources/groups',
        './resources/workflow',
        './resources/notifications',
        
        './resources/regions',
        './resources/house-ranges',
        
        './resources/budget',
        './resources/quickbooks',
//        './resources/version',
	], 
	function (angular) {
		'use strict';
		
		return angular.module('spekit.service').value('version', '0.2');
	}
);