({
	baseUrl : "../lib",
	include : [ "main" ],
	exclude : [ "angular", "jquery" ],
	stubModules : [ 'tpl' ],
	out : "spekit-ng-service.js",

	//wrap: {
	//    "startFile": "wrap.start",
	//    "endFile": "wrap.end"
	//},

	paths : {
		"jquery" : "../bower_components/jquery/dist/jquery",
		"angular" : "../bower_components/angular/angular",
		"spekitService" : "../spekit-ng-service"
	},

	optimize : "none"
})