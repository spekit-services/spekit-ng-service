spekit-ng-service
=================

An Angular module for interacting with the Spekit web service.

# TODO
Separate admin and standard RESTful endpoints.

- Users
- Dwellings (should be no admin?)
- Packages (should be no admin?)
- Price Models (should be no admin?)
- Products (should be no admin?)
